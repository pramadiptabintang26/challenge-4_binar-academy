const express = require('express');
const path = require('path');

const app = express();

app.use(express.static('public'));

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, '../public/index.html'));
});

app.get('/cars', function (req, res) {
    res.sendFile(path.join(__dirname, '../public/cari-mobil.html'));
});

app.listen(8000, 'localhost', () => {
    console.log("Server sudah berjalan, silahkan buka http://localhost:%d", 8000);
});