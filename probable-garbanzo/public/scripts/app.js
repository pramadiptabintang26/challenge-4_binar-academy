class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
  }

  async init() {
    await this.load();

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
  }

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });
  };

  async load() {
    const penumpang = document.getElementById("jumlah-penumpang");
    const tanggal = document.getElementById("tanggal");
    const jam = document.getElementById("waktu-jemput");

    const nilaiPenumpang = penumpang.value;
    const nilaiTanggal = tanggal.value;
    const nilaiJam = jam.value;

    let inputDateTime = Date.parse(nilaiTanggal + "T" + nilaiJam + "Z");
    const cars = await Binar.listCars();

    const availableCar = cars.filter((car) => {
      return car.capacity == nilaiPenumpang && car.available === true && parseInt(Date.parse(car.availableAt)) > parseInt(inputDateTime);
    })

    Car.init(availableCar);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}